import Vue from "vue"
import authStore from "./store/AuthenticationStore"
import {VueFlashMessage} from 'vue-flash-message'

const $eventBus = new Vue();
Vue.prototype.$eventBus = $eventBus;

const interceptors = {
    addToken() {
        Vue.axios.interceptors.request.use(
            (config) => {
                config.headers['Authorization'] = authStore.state.idToken;
                return config;
            },
            (error) => {
                return Promise.reject(error);
            }
        )
    },
    showErrorModal() {
        Vue.axios.interceptors.response.use(
            function (response) {
                return response;
            },
            function (error, th) {
                // handle error
                $eventBus.$emit('open-modal', {
                    type: 'error',
                    message: '...'
                });
                if (error.response) {
                    alert(error.response.data.message);
                }
                return Promise.reject(error);
            });
    }
};

export default interceptors;
