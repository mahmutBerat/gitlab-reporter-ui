import Vue from 'vue'
import Vuex from 'vuex'
import App from './AppReporter.vue'
import router from './router'
import store from './store'
import interceptors from './interceptors'
import websocket from './store/WebSocketStore'
import './registerServiceWorker'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VueResource from 'vue-resource'
import axios from 'axios'
import axiosConfig from './axios'
import VueFlashMessage from 'vue-flash-message';

import VueAxios from 'vue-axios'
import { library } from '@fortawesome/fontawesome-svg-core'
import {faBookOpen,faBookDead, faCoffee} from '@fortawesome/free-solid-svg-icons'
import { faSync } from '@fortawesome/free-solid-svg-icons'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { faWindowClose } from '@fortawesome/free-solid-svg-icons'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { faBinoculars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCoffee);
library.add(faSync);
library.add(faPlus);
library.add(faWindowClose);
library.add(faTimesCircle);
library.add(faBinoculars);
library.add(faBookOpen);
library.add(faBookDead);
// import DataTables and DataTablesServer separately
//import { DataTables, DataTablesServer } from 'vue-data-tables'
//Vue.use(DataTables);
//Vue.use(DataTablesServer);

// import DataTables and DataTablesServer together
import VueDataTables from 'vue-data-tables'
Vue.use(VueDataTables);

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VueAxios, axios);

Vue.use(VueFlashMessage);

Vue.component('font-awesome-icon', FontAwesomeIcon);

interceptors.addToken();
interceptors.showErrorModal();

new Vue({
    router,
    store: new Vuex.Store(store),
    render: h => h(App)
}).$mount('#reporterApp');
