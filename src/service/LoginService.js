import Vue from 'vue';

export default {
    login(params) {
        return Vue.axios.post('http://localhost:8090/login', params).then(function (response) {
            console.info("Success");
            console.info(response.data.Authorization);
            return response;
        })
    },
    signInWithGitlab() {
        return Vue.axios.get('http://localhost:8090/signinwithgitlab')
            .then(response => {
                console.info("SUCCESS");
                console.info(response.data.address);
                window.open(response.data.address, '_blank');
            })
    },

}
