import Vue from 'vue';

export default {
    getIssues(data) {
        let url = `http://localhost:8090/api/issues?projectId=${data.projectId}`;
        return Vue.axios.get(url).then(function (response) {
            console.info("Success getting project issues");
            console.info(response.data);
            return response;
        })
    },
    _createIssue(data) {
        let url = `http://localhost:8090/api/issues`;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        });
    },
    createIssue(data) {
        let url = `http://localhost:8090/api/issueOperation`;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        });
    },
    closeIssue(data) {
        let url = `http://localhost:8090/api/issueOperation/close`;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        });
    },
    _deleteIssue(data) {
        let url = `http://localhost:8090/api/issues/deleteIssue`;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        });
    },
    deleteIssue(data) {
        let url = `http://localhost:8090/api/issueOperation/delete`;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        });
    }
}
