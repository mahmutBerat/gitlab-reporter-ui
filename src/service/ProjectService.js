import Vue from 'vue';

export default {
    fetchUserProjects(data) {
        return Vue.axios.get(
            `http://localhost:8090/api/projects/fetchProjects/?userId=${data.userId}`,
        ).then(function (response) {
            console.info("Success");
            console.info(response.data);
            return response;
        });
    },
    getUserProjects(data) {
        return Vue.axios.get(
            `http://localhost:8090/api/projects/getUserProjects/?userId=${data.userId}`,
        ).then(function (response) {
            console.info("Success");
            console.info(response.data);
            return response;
        });
    }
}
