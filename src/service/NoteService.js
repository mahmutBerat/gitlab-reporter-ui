import Vue from 'vue'

export default {
    api: {"url": "http://localhost:8090/api/notes"},
    getUserNotes(data) {
        let url = `${this.api.url}/?projectId=${data.projectId}&issueId=${data.issueId}`;
        return Vue.axios.get(url).then(function (response) {
            console.info("Success", response.data);
            return response;
        }, function (error) {
            console.info(error);
        });
    },
    fetchNotes(data) {
        let url = `${this.api.url}/fetchIssueNotes?projectId=${data.projectId}&issueId=${data.issueId}`;
        return Vue.axios.get(url).then(function (response) {
            console.info("Success", response.data);
            return response;
        }, function (error) {
            console.info(error);
        });
    },
    createNote(data) {
        let url = this.api.url;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        }, function (error) {

        })
    },
    deleteNote(data) {
        let url = `${this.api.url}/deleteNote`;
        return Vue.axios.post(url, data).then(function (response) {
            console.info("Success", response.data);
            return response;
        }, function (error) {
        })
    }


}
