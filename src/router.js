import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/navbar/Home.vue'
import store from './store'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: "danger",
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/navbar/About.vue')
        },
        {
            path: '/settings',
            name: 'settings',
            component: () => import('./views/navbar/Settings.vue'),
            beforeEnter(to, from, next) {
                if(store.modules.authenticationStore.state.idToken){
                    next();
                }
                else{
                    console.info("Not Authenticated -> First Login!")
                    next('/login');
                }
            }
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/navbar/Login.vue')
        }
    ]
})
