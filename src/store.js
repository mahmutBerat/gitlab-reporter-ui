import AuthenticationStore from "./store/AuthenticationStore";
import ProjectStore from "./store/ProjectStore";
import UserStore from "./store/UserStore";
import IssueStore from "./store/IssueStore";
import NoteStore from "./store/NoteStore";
import websocket from './store/WebSocketStore'

export default {
    modules: {
        authenticationStore: AuthenticationStore,
        projectStore: ProjectStore,
        user: UserStore,
        issueStore: IssueStore,
        noteStore: NoteStore,
        websocketStore: websocket
    }
};
