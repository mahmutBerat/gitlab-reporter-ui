import SockJS from "sockjs-client";
import Stomp from "webstomp-client";
import IssueStore from "./IssueStore";

export default {

    namespaced: true,
    state: {
        issueStore: IssueStore,
        connected: false,
        gitlabHealth: false
    },
    mutations: {
        setGitlabHealth(state, data) {
            state.gitlabHealth = data;
        },
        setConnectionState(state, data){
            state.connected = data
        }

    },
    actions: {
        connectSocket({commit, dispatch, rootGetters}) {
            this.socket = new SockJS("http://localhost:8090/gitlab-reporter-websocket");
            this.stompClient = Stomp.over(this.socket);
            this.stompClient.debug(null);
            this.stompClient.connect(
                {},
                frame => {
                    commit('setConnectionState', true);
                    console.log(frame);
                    this.stompClient.subscribe("/webhook/issue", tick => {
                        dispatch('issueStore/getIssues', {projectId: rootGetters["projectStore/getSelectedProject"]}, {root: true});
                    });
                    this.stompClient.subscribe("/webhook/note", tick => {
                        //TODO update it with note store after completing backend
                        dispatch('issueStore/getIssues', {projectId: rootGetters["projectStore/getSelectedProject"]}, {root: true});
                    });
                    this.stompClient.subscribe("/gitlab/health", tick => {
                        if (tick.body === "OK") {
                            commit('setGitlabHealth', true);
                        } else {
                            commit('setGitlabHealth', false);
                        }
                    });
                },
                error => {
                    console.log(error);
                    commit('setConnectionState', false);
                }
            );
        },
        disconnect() {
            if (this.stompClient) {
                this.stompClient.disconnect();
            }
            commit('setConnectionState', false);
            this.status = "disconnected";
            this.received_messages = [];
        },
        tickleConnection() {
            this.connected ? this.disconnect() : this.connectSocket();
        }
    },
    mounted() {
        this.connectSocket();

    },
    getters: {
        getGitlabHealth(state) {
            return state.gitlabHealth;
        },
        getGitlabServerConnection(state){
            return state.connected;
        }
    },
};
