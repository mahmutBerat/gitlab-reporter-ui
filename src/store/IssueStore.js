import IssueService from './../service/IssueService'

export default {
    namespaced: true,
    state: {
        issues: [],
        selectedIssue: 0
    },
    mutations: {
        addIssuesToState(state, response) {
            state.issues = response.issues;
        },
        setSelectedIssue(state, data) {
            state.selectedIssue = data;
        },
        addToIssues(state, data) {
            console.info(data);
            if(data.status===200){
                state.issues.push(data.data);
            }
        },
        removeIssueFromState(state, data) {
            state.selectedIssue = 0;
            //debugger;
            //state.issues.find(value => value.id == data).
        },
        clear(state){
            state.issues = [];
            state.selectedIssue = 0
        }
    },
    actions: {
        getIssues({commit, dispatch}, data) {
            console.log("IssueStore.getIssue -> to server");
            return IssueService.getIssues(data).then(response => {
                //comes list of issues
                commit('addIssuesToState', {
                    issues: response.data,
                    status: response.status
                })
            })
        },
        selectIssue({commit, dispatch}, data) {
            commit('setSelectedIssue', data);
        },
        deleteIssue({commit}, data) {
            return IssueService.deleteIssue(data).then((response) => {
                commit('removeIssueFromState', {
                    //need projectId, issueId
                    issueId: response.data.id
                })
            });
        },
        closeIssue({commit}, data) {
            return IssueService.closeIssue(data).then((response) => {
                commit('removeIssueFromState', {
                    //need projectId, issueId
                    issueId: response.data.id
                })
            });
        },
        createIssue({commit, dispatch}, data) {
            return IssueService.createIssue(data).then(function (response) {
                commit('addToIssues', response)
            })
        },
        clearStore({commit}){
            commit('clear');
        }
    },
    getters: {
        getAllIssues(state) {
            return state.issues;
        },
        getSelectedIssue(state) {
            return state.selectedIssue;
        },
        getSelectedIssueObject({getter}, state) {
            //debugger;
            return state.issues && state.issues.find(value => value.id === state.selectedIssue.id);
        }
    }
}
