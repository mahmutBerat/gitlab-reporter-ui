import LoginService from '../service/LoginService'

import router from './../router'

export default ({
    namespaced: true,
    state: {
        idToken: null,
        authStatus: null,
        user: {
            userId: null,
            userName: null,
            fullName: null,
            bio: null,
            avatarUrl: null
        },
    },
    mutations: {
        authUser(state, response) {
            state.authStatus = response.status;
            if (state.authStatus === 200) {
                state.idToken = response.token;
                state.user = {...response.user}
            }
        },
        clearAuthData(state) {
            state.idToken = null;
            state.userId = null;
            state.userName = null;
            state.authStatus = null;
            state.user = {};
        }
    },
    actions: {
        login({commit, dispatch}, data) {
            return LoginService.login(data).then(res => {
                commit('authUser', {
                    status: res.status,
                    token: res.data.Authorization,
                    user: {
                        userName: res.data.userName,
                        userId: res.data.userId,
                        fullName: res.data.fullName,
                        bio: res.data.bio,
                        avatarUrl: res.data.avatarUrl
                    }
                });
                dispatch('websocketStore/connectSocket', {}, {root: true});
            });
        },
        gitlabSignIn({commit, dispatch}) {
            return LoginService.signInWithGitlab().then(res => {
                commit('authUser', {
                    token: res.data.token,
                    userId: res.data.userId
                })
            });
        },
        logout({commit, dispatch}) {
            commit('clearAuthData');
            dispatch('projectStore/clearStore', {},{root:true});
            dispatch('issueStore/clearStore', {},{root:true});
            dispatch('noteStore/clearStore', {},{root:true});
            router.replace('/login')
        }
    },
    getters: {
        getAuthToken(state) {
            return state.idToken;
        },
        isAuthenticated(state) {
            return state.idToken !== null;
        },
        getAuthenticatedUser(state) {
            return state.user;
        },
        getAuthenticatedUserId(state) {
            return state.user.userId;
        }
    }
})
