import ProjectService from '../service/ProjectService'

export default ({
    namespaced: true,
    state: {
        projects: [],
        selectedProject: 0
    },
    mutations: {
        setProjects(state, response) {
            state.projects = response.projects
        },
        setSelectedProject(state, data) {
            state.selectedProject = data;
        },
        clear(state){
            state.projects = [];
            state.selectedProject = 0;
        }
    },
    actions: {
        clearStore({commit}){
            commit('clear');
        },
        getUserProjects({commit, dispatch}, data) {
            return ProjectService.getUserProjects(data)
                .then(response => {
                    commit('setProjects', {
                        status: response.status,
                        projects: response.data
                    })
                });
        },
        selectProject({commit, dispatch}, data) {
            commit('setSelectedProject', data);
            dispatch('issueStore/selectIssue', 0, {root:true});
            dispatch('noteStore/selectNote', 0, {root:true});
        }
    },
    getters: {
        getAllProjects(state) {
            return state.projects;
        },
        getSelectedProject(state) {
            return state.selectedProject;
        },
        getSelectedProjectObject(state){
            return state.projects.find(value => value.projectId === state.selectedProject);
        }
    }
})
