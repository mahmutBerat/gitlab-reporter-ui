import UserService from '../service/UserService'

export default ({
    namespaced: true,
    state: {},
    mutations: {},
    actions: {
        login(id, {dispatch}) {
            return UserService.getUser(id);
        }
    }
})
