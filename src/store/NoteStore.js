import NoteService from "../service/NoteService"

export default {
    namespaced: true,
    state: {
        notes: [],
        selectedNote: 0
    },
    mutations: {
        setNotes(state, data) {
            state.notes = data;
        },
        setSelectedNote(state, data) {
            state.selectedNote = data;
        },
        addToNotes(state, data) {
            console.info(data);
            state.notes.push(data);
        },
        removeFromNotes(state, data) {
            debugger;
            console.info(data);
            if(data && data.status === 200){
                //remove it from store

            }
        },
        clear(state){
            state.notes = [];
            state.selectedNote = 0
        }
    },
    actions: {
        getUserNotes({commit, dispatch, rootGetters}) {
            const data = {
                projectId: rootGetters['projectStore/getSelectedProject'],
                issueId: rootGetters['issueStore/getSelectedIssue']
            };
            return NoteService.getUserNotes(data).then(function (response) {
                commit('setNotes', response.data);
            })
        },
        fetchNotes({commit, dispatch, rootGetters}) {
            const data = {
                projectId: rootGetters['projectStore/getSelectedProject'],
                issueId: rootGetters['issueStore/getSelectedIssue']
            };
            return NoteService.fetchNotes(data).then(function (response) {
                commit('setNotes', response.data);
            })
        },
        selectNote({commit}, data) {
            commit('setSelectedNote', data);
        },
        createNote({commit}, data) {
            return NoteService.createNote(data).then(function (response) {
                commit('addToNotes', response.data);
            })
        },
        deleteNote({commit}, data){
            return NoteService.deleteNote(data).then(function (response) {
                commit('removeFromNotes', response);
            })
        },
        clearStore({commit}){
            commit('clear');
        }
    },
    getters: {
        getNotes(state) {
            return state.notes;
        },
        getSelectedNote(state) {
            return state.selectedNote;
        }

    }
}
