import axios from 'axios';

const interceptors = {
        request: () => {
            axios.interceptors.request.use(function (config) {
                // Do something before request is sent
                //add token before going server
                return config
            }, function (error) {
                // Do something with request error
                return Promise.reject(error);
            });
        }
    };


export default {
    configure() {
        interceptors.request({});
    }
}
